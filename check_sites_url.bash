#!/bin/bash

function check_url {

  if [ -f "$PWD/$1" ];then

    while read line;do
      response_code=`"$(which curl)" -s -o /dev/null $line -w "%{http_code}" `
      if [[ $response_code != 4[0-9][0-9] ]] && [[ $response_code != 5[0-9][0-9] ]];then
        echo -e "\nsite $line is alive"
        echo "responce code = $response_code"
      else
        echo -e "\nsite $line is dead"
        break
      fi
      done < "$PWD/$1"
  else
    echo "there is no file with urls"
  fi
}

check_url "urls.txt"